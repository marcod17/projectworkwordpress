<div class="copyright-wrapper">
        	<div class="inner">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="contenuto-footer">
                                <div class="row logo-footer"></div>
                                <div class="row mail">
                                    <p>E-mail: promozione@comune.rimini.it</p>
                                </div>
                                <div class="row telefono">
                                    <p>Telefono: 0541 101564/101568</p>
                                </div>
                                <div class="row copywright">
                                    <p>Sviluppato da: <a href="www.marcodelbianco.it">Marco ©2020</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>         
            </div><!-- inner -->
        </div>
    </div>
<a href="#" class="scrollToTop"></a>  
<?php wp_footer(); ?>

</body>
</html>