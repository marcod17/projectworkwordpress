<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Precious Lite
 */

get_header(); ?>

<div class="content-area">
    <div class="middle-align content_sidebar">
        <div class="site-main" id="sitemain">
            <style>
                .scopri{background: red; color:white; padding:10px 20px;}
                .scopri a {color:white;font-size:15px;}
                .post-thumb img {height:400px;width: 600px;}
                @media screen and (max-width: 1159px) and (min-width: 1000px){
                    .site-main {width: 750px;}
                }
                @media screen and (max-width: 720px;) and (min-width: 300px;){
                    .post-thumb img {height:200px !important;width: 300px !important;}
                }     
            </style>
			<?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', 'single' ); ?>
                <?php
                    /* Aggiungo custom fields fonte */
                    $data = get_post_meta(get_the_ID(),'data',true);
			        $luogo = get_post_meta(get_the_ID(), 'luogo', true);
			        $tipologia = get_post_meta(get_the_ID(), 'tipologia', true);
                    $prezzo = get_post_meta(get_the_ID(), 'prezzo', true);
                    $url = get_post_meta(get_the_ID(), 'urlevento', true);
                ?>
                    <div class="custom-tags">
                        <?php 
                            echo("<p>L'evento si svolgerà il: ".$data."</p>");
			                echo("<p>Presso: ".$luogo."</p>");
			                echo("<p>Sarà una: ".$tipologia."</p>");
                            echo("<p>Prezzo: ".$prezzo."€</p>");
                            echo("<p>Per saperne di più: <a href=".$url.">clicca qui</a>"); 
                        ?>
                    </div>
                
                <?php precious_lite_content_nav( 'nav-below' ); ?>
                
                <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() )
                    comments_template();
                ?>
                
            <?php endwhile; // end of the loop. ?>
        </div>
        <?php get_sidebar();?>
        <div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>