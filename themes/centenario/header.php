<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package centenario
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text"
            href="#content"><?php esc_html_e( 'Skip to content', 'centenario' ); ?></a>

        <header id="masthead" class="site-header">
            <nav class="navbar navbar-light bg-light">
                <a class="navbar-brand" href="#">
                    <img src="/img/regista.jpg" width="150" height="130"
                        class="d-inline-block align-top" alt="">
						<h1 style="font-size: 30px;"><?php esc_attr(bloginfo( 'name' )); ?></h1>
                </a>
				<div class="navbar-nav">
					<?php wp_nav_menu( array('theme_location'  => 'primary') ); ?>
    			</div>
            </nav>
            <!-- <div class="site-branding"> -->
                <?php
			// the_custom_logo();
			// if ( is_front_page() && is_home() ) :
				?>
                <!-- <h1 class="site-title"><a href="<?php// echo esc_url( home_url( '/' ) ); ?>"
                        rel="home"><?php// bloginfo( 'name' ); ?></a></h1> -->
                <?php 
			// else :
				?>
                <!-- <p class="site-title"><a href="<?php// echo esc_url( home_url( '/' ) ); ?>"
                        rel="home"><?php// bloginfo( 'name' ); ?></a></p>
                <?php
			//endif; -->
			//$centenario_description = get_bloginfo( 'description', 'display' );
			//if ( $centenario_description || is_customize_preview() ) :
				?>
                <p class="site-description"><?php// echo $centenario_description; /* WPCS: xss ok. */ ?></p>
                <?php //endif; ?>
            </div>
			<!-- .site-branding -->

            <!-- <nav id="site-navigation" class="main-navigation">
                <button class="menu-toggle" aria-controls="primary-menu"
                    aria-expanded="false"><?php //esc_html_e( 'Primary Menu', 'centenario' ); ?></button> -->
                <?php
			// wp_nav_menu( array(
			// 	'theme_location' => 'menu-1',
			// 	'menu_id'        => 'primary-menu',
			// ) );
			?>
            </nav><!-- #site-navigation -->
        </header><!-- #masthead -->

        <div id="content" class="site-content">