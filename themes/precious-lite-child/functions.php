<?php
	add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' ); // specifica che va fatta quando c'è il caricamento degli script, si trova nella pagina delle action
	function my_theme_enqueue_styles() {
    		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); // qui gli specifichiamo di aggiungere il foglio di stile del tema padre mentre carica gli script
	}
	
	