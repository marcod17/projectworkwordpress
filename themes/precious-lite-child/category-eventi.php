<?php 
/**  
 * * Template Name: Eventi  
 * * Template for "events" archive  
 * *  
*/
 
get_header();
?> 
<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title" style="color:red">', '</h1>' );
					// the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header>
            
<div id="primary" class="content-area">   
<main id="main" class="site-main" role="main">    
<div id="content" class="container article-container">      
<header class="event-header">      
<h1 class="event-title ">       
    <?php the_title(  ); ?>      
</h1>     </header>      
<?php      //load and show events excerpt      
$args = array('post_type' => 'evento', 'posts_per_page' => 10 );      
$loop = new WP_Query($args);            
while ($loop->have_posts()) : $loop->the_post();       
?>        
<div >        
    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">         
        <div style="background-image: url(<?php echo the_post_thumbnail_url('medium'); ?>)" >         
        </div>       
    </a>        
    <h2>        
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
            <?php the_title(); ?>
        </a>
    </h2>        
    <!-- <div class="jnj-event-archive-excerpt hidden-sm hidden-xs">
        <?php// the_excerpt(); ?></div>       
    </div>         -->
<?php      endwhile;            
?>      
</div><!--.article-container -->     
    <?php wp_reset_query(); ?>    
    </div><!--.row -->  
    </div><!-- .container role: main -->  
    </div><!--#main-wrapper"-->   
    </main><!-- #main -->  
    </div><!-- #primary -->  
