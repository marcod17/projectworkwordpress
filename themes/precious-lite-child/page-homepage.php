<?php
/*
*
Template Name: Homepage
*/
get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <div class="container">
        <div class="row main-row">
            
            <div class="col-md-8 eventi">
            <h1 class="titolo-eventi">
                EVENTI
            </h1>
                <div class="sfondo-eventi">
                <div class="row scheda-evento primo-evento">
                    <?php 
                        $args = array(  
                            'post_type' => 'evento',
                            'post_status' => 'publish',
                            'posts_per_page' => 1, 
                        );
                        $loop = new WP_Query( $args );
                    ?>
                    <div class="col-md-4 immagine-evento">
                        <div class="well">
                            <?php 
                                while ( $loop->have_posts() ) : $loop->the_post();
                                the_post_thumbnail();
                                endwhile;
                            ?>
                        </div>
                    </div>
                    <div class="col-md-8 testo-evento">
                        <div class="well scheda-testo-evento">
                        <?php 
                            while ( $loop->have_posts() ) : $loop->the_post(); 
                        ?> 
                            <h3 class="titolo-scheda"><a href="<?php get_permalink() ?>"><?php print the_title(); ?></a></h3>
                        <?php 
                            the_excerpt();
                           
                            endwhile;
                            wp_reset_postdata();
                        ?> 
                        </div>
                    </div>
                </div>
                <div class="row scheda-evento secondo-evento">
                    <?php 
                        $args = array(  
                            'post_type' => 'evento',
                            'post_status' => 'publish',
                            'posts_per_page' => 2, 
                        );
                        $loop = new WP_Query( $args );
                    ?>
                    <div class="col-md-4 immagine-evento">
                        <div class="well immagine-evento">
                            <?php 
                                $i = 0; 
                                while ( $loop->have_posts() ) : $loop->the_post();
                                if ($i == 0){$i++;} else {the_post_thumbnail();}
                                endwhile;
                                wp_reset_query();
                            ?>
                        </div>
                    </div>
                    <div class="col-md-8 testo-evento">
                        <div class="well scheda-testo-evento">
                        <?php
                            $i = 0;  
                            while ( $loop->have_posts() ) : $loop->the_post();
                            if ($i == 0){$i++;} else {
                        ?> 
                            <h3 class="titolo-scheda"><a href="<?php get_permalink() ?>"><?php print the_title(); ?></a></h3>
                        <?php 
                            the_excerpt();
                            }  
                            endwhile;

                            wp_reset_postdata();
                        ?> 
                        </div>
                    </div>
                </div>
                
                <div class="row scheda-evento terzo-evento">
                    <?php 
                        $args = array(  
                            'post_type' => 'evento',
                            'post_status' => 'publish',
                            'posts_per_page' => 3, 
                        );
                        $loop = new WP_Query( $args );
                    ?>
                    <div class="col-md-4 immagine-evento">
                        <div class="well">
                            <?php 
                                $i = 0; 
                                while ( $loop->have_posts() ) : $loop->the_post();
                                if ($i == 0 || $i == 1){$i++;} else {the_post_thumbnail();}
                                endwhile;
                                wp_reset_query();
                            ?>
                        </div>
                    </div>
                    <div class="col-md-8 testo-evento">
                        <div class="well scheda-testo-evento">
                        <?php
                            $i = 0;  
                            while ( $loop->have_posts() ) : $loop->the_post();
                            if ($i == 0 || $i ==1){$i++;} else {
                        ?> 
                            <h3 class="titolo-scheda"><a href="<?php get_permalink() ?>"><?php print the_title(); ?></a></h3>
                        <?php 
                            the_excerpt();
                            }  
                            endwhile;

                            wp_reset_postdata();
                        ?> 
                        </div>
                    </div>
                </div>
                </div>
                
        </div>
        <div class="col-md-4 film">
            <h1 class="titolo-film">
                OPERE
            </h1>
                <div class="sfondo-film">
                <div class="well scheda-evento">  
                <?php 
                        $args = array(  
                            'post_type' => 'film',
                            'post_status' => 'publish',
                            'posts_per_page' => 1, 
                            'orderby' => 'date', 
                            'order' => 'DESC', 
                        );
                        $loop = new WP_Query( $args ); 
                        while ( $loop->have_posts() ) : $loop->the_post(); 
                        ?>
                        <h3 class="titolo-scheda-film"><?php print the_title(); ?></h3>
                        <?php     
                            echo "<br>";
                            the_post_thumbnail();
                            the_excerpt(); 
                            $data = get_post_meta(get_the_ID(),'data',true);
                            $durata = get_post_meta(get_the_ID(), 'durata', true);
                        ?>
                            <div class="custom-film col-md-12">
                                <div class="row">
                                    <div class="custom-field-film col-md-8">
                                        <?php 
                                            echo("<p>Anno: ".$data."</p>");
                                            echo("<p>Durata: ".$durata." min.</p>");
                                        ?>
                                    </div>
                                    <div class="col-md-4 button-film">
                                        <button class="scopri"><a href="<?php echo get_permalink() ?>">Scopri</a></button>
                                    </div>
                                </div>
                                
                            </div>
                        <?php    
                        endwhile;

                        wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->
<div class="clear"></div>
<div class="bio col-md-12 bio">
    <div class="row">
        <div class="col-md-4 bio-statica">
            <div style="margin: auto;">
                <h2 class="titolo-bio-statica">Federico Fellini</h2>
                <br>
                <p>
                    (Rimini, 20 gennaio 1920 – Roma, 31 ottobre 1993) è stato un regista, sceneggiatore, fumettista e scrittore italiano. Definiva se stesso "un artigiano che non ha niente da dire, ma sa come dirlo". 
                </p>
            </div>
            <!-- Breve autobiografia -->
            
        </div>
        <?php 
            $args = array(  
                'post_type' => 'bio',
                'post_status' => 'publish',
                'posts_per_page' => 1, 
                'orderby' => 'date',
                'order' => 'DESC',
                );
            $loop = new WP_Query( $args );
        ?>
        <div class="col-md-4 thumbnail-bio">
            <!-- immagine ultimo articolo di tipo bio  -->
            <?php 
                while ( $loop->have_posts() ) : $loop->the_post();
                the_post_thumbnail();
                endwhile;
            ?>
        </div>
        <div class="col-md-4 scheda-testo-bio">
            <!-- ultimo articolo di tipo bio  -->
            <?php 
                while ( $loop->have_posts() ) : $loop->the_post(); 
            ?> 
            <h3 class="titolo-scheda-bio"><a href="<?php echo get_permalink() ?>"><?php print the_title(); ?></a></h3>
            <div class="testo-scheda-bio">
                <?php 
                    the_excerpt(); 
                    endwhile;

                    wp_reset_postdata();
                ?>
            </div>
            
        </div>
    </div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>
